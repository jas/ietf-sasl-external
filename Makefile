all: draft-josefsson-sasl-external-channel.txt draft-josefsson-sasl-external-channel.html draft-josefsson-sasl-external-channel-from--04.diff.html

clean:
	rm *~ draft-josefsson-sasl-external-channel.txt draft-josefsson-sasl-external-channel.html

draft-josefsson-sasl-external-channel.txt: draft-josefsson-sasl-external-channel.xml
	xml2rfc $<

draft-josefsson-sasl-external-channel.html: draft-josefsson-sasl-external-channel.xml
	xml2rfc $< $@

draft-josefsson-sasl-external-channel-from--04.diff.html: draft-josefsson-sasl-external-channel.txt
	rfcdiff draft-josefsson-sasl-external-channel-04.txt draft-josefsson-sasl-external-channel.txt 
